﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Credits : MonoBehaviour {

	public float Speed;
	public Transform Center;
	public bool IsCenter;

	void Update () {
		if (!IsCenter) transform.position += transform.up * Speed * Time.deltaTime;
		else{
			if(transform.position.y < Center.transform.position.y)
				transform.position += transform.up * Speed * Time.deltaTime;
		}
	}

	public void Return(){
		Application.LoadLevel("MainMenu");
	}
}
