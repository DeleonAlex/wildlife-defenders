﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour
{
	public List<string> GameScenes;
    public Button StartButton;
    public Button CreditsButton;
    public Button Quit;
	public GameObject Splash;

    public void StartGame()
    {
        //instantiate character
        //instantiate levelmanager
		var rand = (int)(Random.Range(0, GameScenes.Count));
		StartCoroutine(SceneSelection(GameScenes[rand]));
    }
    public void Credits()
    {
		Application.LoadLevel("Credits");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

	IEnumerator SceneSelection(string level){
		AsyncOperation gameLevel = Application.LoadLevelAsync(level);
		while(!gameLevel.isDone){
			Splash.SetActive(true);
			yield return null;
		}
	}
}
