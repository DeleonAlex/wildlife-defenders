﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public string LevelToLoad;
    public Text LoadProgressText;
    public GameObject Image;

    public float Count;
    private float counter;

    private int LoadProgress = 0;

    void Awake()
    {
        counter = 0;
    }

    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(LoadLevel(LevelToLoad));
        }
    }

    IEnumerator LoadLevel(string level)
    {
        AsyncOperation loadingScene = Application.LoadLevelAsync(level);
        while (!loadingScene.isDone && counter <= Count )
        {
            counter += Time.deltaTime;
            Image.SetActive(true);
            LoadProgress = (int)(loadingScene.progress * 100);
            LoadProgressText.text = LoadProgress + "%";
            yield return null;
        }
    }
}
