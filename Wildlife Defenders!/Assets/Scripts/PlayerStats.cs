﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    public int Difficulty;
    public int PlayerScore;

    public int ShowScore
    {
        get
        {
            return PlayerScore;
        }
    }

}
