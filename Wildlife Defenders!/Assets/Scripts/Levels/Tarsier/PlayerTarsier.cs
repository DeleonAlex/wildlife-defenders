﻿using UnityEngine;
using System.Collections;

public class PlayerTarsier : MonoBehaviour {

	void Update()
	{
		PlayerMovement ();
	}
	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey("GAMESPEED");
	}

	void PlayerMovement()
	{
		GameObject player = GameObject.Find ("Player");
		if (Input.GetKey(KeyCode.A))
		{
			player.transform.position = new Vector3(-2,player.transform.position.y, player.transform.position.z);
		}
		if (Input.GetKey(KeyCode.D))
		{
			player.transform.position = new Vector3(2,player.transform.position.y, player.transform.position.z);
		}
	}
}
