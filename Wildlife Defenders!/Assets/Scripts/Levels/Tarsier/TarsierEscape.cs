﻿	using UnityEngine;
using System.Collections	;

public class TarsierEscape : MonoBehaviour {
	

	private float fallSpeed;

	void Start()
	{
		fallSpeed = 5.9f;
	}

	void Update()
	{	
		ContinuousMovement ();
	}

	void ContinuousMovement()
	{
		transform.localPosition -= transform.up * fallSpeed * Time.deltaTime;
	}

	void TarsierObstacle()
	{
		//	Instantiate(obstaclePrefab, Random.Range(0, spawnWaypoints.Length), Quaternion.identity);
	}


	

}
