﻿using UnityEngine;
using System.Collections;

public class StopTimer: GameEvent{
}

public class RemainingBombs : MonoBehaviour {

	public int Bombs;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		Bombs = transform.childCount;
		if (Bombs <= 0){
			this.RaiseEventGlobal<StopTimer>(new StopTimer());
		}
	}
}
