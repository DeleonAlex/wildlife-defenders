﻿using UnityEngine;
using System.Collections;

public class HPReduction: GameEvent{
}
public class ResetDuration: GameEvent{
}

public class UnitCollision : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Bomb")){
			this.RaiseEventGlobal<ResetDuration>(new ResetDuration());
		}

	}
}
