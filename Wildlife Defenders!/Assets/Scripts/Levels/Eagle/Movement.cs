﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float CloudSpeed;

	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey("GAMESPEED");
	}

	void Update () {
		transform.position += new Vector3(-1, 0, 0) * CloudSpeed * Time.deltaTime;
	}
}
