﻿using UnityEngine;
using System.Collections;

public class Flap : MonoBehaviour {

	public float Force;

	void OnEnable(){
		this.AddEventListenerGlobal<HideAvatar>(HideObj);
	}
	
	void OnDisable(){
		this.RemoveEventListenerGlobal<HideAvatar>(HideObj);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Mouse0)){
			GetComponent<Rigidbody2D>().AddForce(Vector2.up * Force);
		}
	}

	void HideObj(HideAvatar e){
		this.gameObject.SetActive(false);
	}
}
