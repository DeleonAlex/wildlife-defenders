﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour {

	public float Force;
	private float _speed;
	bool canJump = true;
	bool inGround = true;
    public GameObject Player;
	

	void Start()
	{
		_speed = 3.0f;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.CompareTag("Iceberg"))
			inGround = true;
			canJump = true;
	}

	// Update is called once per frame
	void Update () {
		if (canJump == true) {
			if (Input.GetKeyDown (KeyCode.Space)) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * Force);
				inGround = false;
			}
		if(inGround == false)
			{
				canJump = false;
			}

		}
		PlayerMovement ();
	}

	void PlayerMovement()
	{
		if (Input.GetKey(KeyCode.A))
		{
			Player.transform.position += Vector3.left *_speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.D))
		{
			Player.transform.position += Vector3.right *_speed * Time.deltaTime;
		}
	}

}
