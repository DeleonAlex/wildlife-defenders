﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneSelect : MonoBehaviour {

	public List<string> GameScenes;
	public List<GameObject> PlayerScoreObjComponents;
	private float counter;
	public float SceneDelay;

	private bool speedUpdated;

	private int rand;

	void Awake(){
		rand = (int)(Random.Range(0, GameScenes.Count));
	}

	void OnEnable(){
		this.AddEventListenerGlobal<NextLevel>(SelectLevel);
	}
	void OnDisable(){
		this.RemoveEventListenerGlobal<NextLevel>(SelectLevel);
	}

	void SelectLevel(NextLevel e){
		if (!PlayerPrefs.HasKey("GAMESPEED"))PlayerPrefs.SetFloat("GAMESPEED", 0);
		if(!speedUpdated){
			var temp = PlayerPrefs.GetFloat("GAMESPEED");
			temp += (10 * Time.deltaTime);             
			PlayerPrefs.SetFloat("GAMESPEED", temp);
			speedUpdated = true;
		}
		StartCoroutine(LoadLevel(GameScenes[rand]));
	}

	IEnumerator LoadLevel(string level){
		if(counter < SceneDelay) {
			foreach (GameObject obj in PlayerScoreObjComponents){
				obj.SetActive(true);
			}
			counter += Time.deltaTime;
		}
		else{
			Application.LoadLevel(level);
			yield return null;
		}
	}
}
