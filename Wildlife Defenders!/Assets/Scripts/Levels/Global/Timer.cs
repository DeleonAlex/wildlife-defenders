﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextLevel : GameEvent{
}

public class AddScore : GameEvent{
}
public class HideAvatar: GameEvent{
}
public class UpdateHPUI: GameEvent{
}

public class Timer : MonoBehaviour {

	public RectTransform Slider;

	private float duration;
	public float Duration;
	private float timerNormalized{
		get{ return duration / Duration; }
	}

	private bool lvlFail;

	private bool hpReduced;

	public bool TimeBound; //time must reach zero?

	// Use this for initialization
	void Start () {
		Duration -= PlayerPrefs.GetFloat("GAMESPEED");
		duration = Duration;
	}

	void OnEnable(){
		this.AddEventListenerGlobal<ResetDuration>(OnHpReduction);
		this.AddEventListenerGlobal<StopTimer>(FreezeTime);
	}
	void OnDisable(){
		this.RemoveEventListenerGlobal<ResetDuration>(OnHpReduction);
		this.RemoveEventListenerGlobal<StopTimer>(FreezeTime);
	}
	
	// Update is called once per frame
	void Update () {

		if (duration > 0){
			duration -= Time.deltaTime;
			Vector2 temp = Slider.localScale;
			temp.x = timerNormalized;
			Slider.localScale = temp;
		}
		else {
			if (!lvlFail && TimeBound)this.RaiseEventGlobal<AddScore>(new AddScore());
			this.RaiseEventGlobal<NextLevel>(new NextLevel());
			this.RaiseEventGlobal<HideAvatar>(new HideAvatar());
			if (!hpReduced && (lvlFail || !TimeBound)){
				this.RaiseEventGlobal<HPReduction>(new HPReduction());
				hpReduced = true;	
			}
			this.RaiseEventGlobal<UpdateHPUI>(new UpdateHPUI());
		}
	}

	void OnHpReduction(ResetDuration e){
		lvlFail = true;
		duration = 0;
	}

	void FreezeTime(StopTimer e){
		duration = Duration;
		this.RaiseEventGlobal<AddScore>(new AddScore());
		this.RaiseEventGlobal<NextLevel>(new NextLevel());
		this.RaiseEventGlobal<UpdateHPUI>(new UpdateHPUI());
	}
}
