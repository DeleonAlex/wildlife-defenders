﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public float PlayerScore;
	public Text ScoreText;
	private float LevelReward;
	private float rewardScore;

	void OnEnable(){
		this.AddEventListenerGlobal<AddScore>(UpdateScore);
	}
	void OnDisable(){
		this.RemoveEventListenerGlobal<AddScore>(UpdateScore);
	}

	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey("SCORE");
	}

	void Start () {
		rewardScore = 0;
		PlayerScore = PlayerPrefs.GetInt("SCORE");
		ScoreText.text = PlayerScore.ToString();
		LevelReward = Random.Range(100,500);
	}

	void UpdateScore(AddScore e){
		if (rewardScore < LevelReward){
			PlayerScore+= 10;
			rewardScore+= 10;
			ScoreText.text = PlayerScore.ToString();
			PlayerPrefs.SetInt("SCORE", (int)(PlayerScore));
		}
	}
}
