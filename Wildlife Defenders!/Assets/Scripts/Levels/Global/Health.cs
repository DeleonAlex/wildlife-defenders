﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Health : MonoBehaviour {

	[SerializeField]
	private int HP;

	public List<Image> HealthCol;

	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey("HP");
	}

	void Awake(){
		if(!PlayerPrefs.HasKey("HP")) PlayerPrefs.SetInt("HP", 3);
		HP = PlayerPrefs.GetInt("HP");
	}

	void OnEnable(){
		this.AddEventListenerGlobal<HPReduction>(ReduceHP);
		this.AddEventListenerGlobal<UpdateHPUI>(HealthUIUpdate);
	}

	void OnDisable(){
		this.RemoveEventListenerGlobal<HPReduction>(ReduceHP);
		this.RemoveEventListenerGlobal<UpdateHPUI>(HealthUIUpdate);
	}

	void HealthUIUpdate(UpdateHPUI e){
		for(int i = 0; i < HP; i++){
			HealthCol[i].color = Color.white;
		}
	}

	void ReduceHP(HPReduction e){
		if (HP > 1){
			HP -= 1;
			PlayerPrefs.SetInt("HP", HP);
		}
		else {Application.LoadLevel("GameOver");}
	}
}
