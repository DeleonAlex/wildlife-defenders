﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetPicker : MonoBehaviour {

	public List<GameObject> ObjSets;

	void Start () {
		var rand = (int)(Random.Range(0, ObjSets.Count));
		ObjSets[rand].SetActive(true);
	}
}
