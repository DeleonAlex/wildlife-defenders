﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreTally : MonoBehaviour {

	public Text HighScoreText;
	public Text ScoreText;
	[SerializeField]
	private float highScore;
	[SerializeField]
	private float playerScore;

	void Start () {
		playerScore = PlayerPrefs.GetInt("SCORE");
		highScore = PlayerPrefs.GetInt("HSCORE");
		if (playerScore > highScore) {
			highScore = playerScore;
		}

		HighScoreText.text = highScore.ToString();
		ScoreText.text = playerScore.ToString();
	}

	void OnApplicationQuit(){
		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetInt("HSCORE", (int)(highScore));
	}

	public void MainMenu(){
		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetInt("HSCORE", (int)(highScore));
		Application.LoadLevel("MainMenu");
	}
}
