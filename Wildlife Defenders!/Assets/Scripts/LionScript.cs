﻿using UnityEngine;
using System.Collections;

public class LionScript : MonoBehaviour {

    public float Speed;
    public Transform Lion;
	
	// Update is called once per frame
	void Update () {

        Lion.Translate(Vector3.right * Speed*Time.deltaTime);
	}
}
